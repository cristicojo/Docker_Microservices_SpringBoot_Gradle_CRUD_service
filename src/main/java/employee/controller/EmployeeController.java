package employee.controller;

import java.util.List;
import java.util.Map;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import employee.entity.Employee;
import employee.service.EmployeeService;


@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@CrossOrigin
public class EmployeeController {

  private final EmployeeService service;


  @GetMapping(value = "/find/all")
  public ResponseEntity<List<Employee>> findAll() {
    return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
  }


  @GetMapping(value = "/employee/{id}")
  public ResponseEntity<Employee> findById(@PathVariable(value = "id") String id) {
    return new ResponseEntity<>(service.getById(id), HttpStatus.OK);
  }


  @PostMapping(value = "/create")
  public ResponseEntity<Employee> save(@RequestBody Employee emp) {
    return new ResponseEntity<>(service.create(emp), HttpStatus.CREATED);
  }


  @PostMapping(value = "/create/all")
  public ResponseEntity<List<Employee>> saveAll(@RequestBody List<Employee> employeeList) {
    return new ResponseEntity<>(service.createAll(employeeList), HttpStatus.CREATED);
  }


  @PutMapping(value = "/employee/{id}")
  public ResponseEntity<Employee> update(@RequestBody Employee newEmp, @PathVariable(value = "id") String id) {
    return new ResponseEntity<>(service.updateById(id, newEmp), HttpStatus.OK);
  }


  @DeleteMapping(value = "/employee/{id}")
  public ResponseEntity<Map<String, Object>> deleteById(@PathVariable(value = "id") String id) {
    return new ResponseEntity<>(service.remove(id),HttpStatus.OK);
  }


  @DeleteMapping(value = "/remove/all")
  public ResponseEntity<Map<String, Object>> deleteAll() {
    return new ResponseEntity<>(service.removeAll(), HttpStatus.OK);
  }


  //the employee who has the biggest salary in the given department
  @GetMapping(value = "/max/{department}")
  public ResponseEntity<Employee> findByByDepartmentMaxSalary(@PathVariable(value = "department") String department) {
    return new ResponseEntity<>(service.maxSalary(department), HttpStatus.OK);
  }


  //the manager who has the most "direct" employees coordinated by him
  @GetMapping(value = "/manager")
  public ResponseEntity<Employee> getManager() {
    return new ResponseEntity<>(service.getDirectManager(), HttpStatus.OK);
  }


  //BONUS POINTS
  //load a json file
  @GetMapping(value = "/upload")
  public ResponseEntity<Map<String, Object>> uploadFile() {
    return new ResponseEntity<>(service.importJsonFile(), HttpStatus.OK);
  }


  //paging
  @GetMapping(value = "/page={page}&size={size}")
  public ResponseEntity<Page<Employee>> loadPage(@PathVariable(value = "page") int page,
      @PathVariable(value = "size") int size) {
    return new ResponseEntity<>(service.getPage(page, size), HttpStatus.OK);
  }


  //top n best paid employees in a given department
  @GetMapping(value = "/top/{department}/{n}")
  public ResponseEntity<List<Employee>> topN(@PathVariable(value = "department") String department,
      @PathVariable(value = "n") int n) {
    return new ResponseEntity<>(service.topBestPaid(department, n), HttpStatus.OK);
  }


  //management tree: from the top CEO to the lowest employee
  @GetMapping(value = "/tree")
  public ResponseEntity<List<Employee>> tree() {
    return new ResponseEntity<>(service.managementTree(), HttpStatus.OK);
  }
}

