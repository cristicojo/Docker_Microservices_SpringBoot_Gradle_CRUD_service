package employee.service;

import employee.CRUDServiceApplication;
import employee.entity.Employee;
import employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class EmployeeService {

  private final EmployeeRepository repo;
  private final MongoTemplate mongoTemplate;



  public List<Employee> getAll() {
    return repo.findAll();
  }

  public Employee getById(String id) {
    return repo.findById(id).orElseThrow(() ->
        new ResourceNotFoundException("Could not found an employee with id: " + id));
  }


  public Employee create(Employee newEmp) {
    return repo.save(newEmp);
  }


  public List<Employee> createAll(List<Employee> employeeList) {
    return repo.saveAll(employeeList);
  }


  public Employee updateById(String id, Employee newEmp) {

    Employee oldEmp = getById(id);
    oldEmp.setFirstName(newEmp.getFirstName());
    oldEmp.setLastName(newEmp.getLastName());
    oldEmp.setDob(newEmp.getDob());
    oldEmp.setDirectManager(newEmp.getDirectManager());
    oldEmp.setSalary(newEmp.getSalary());
    oldEmp.setDepartment(newEmp.getDepartment());
    return repo.save(oldEmp);
  }


  public Map<String, Object> remove(String id) {
    repo.delete(getById(id));

    Map<String, Object> body = new LinkedHashMap<>();
    body.put("message", "Employee with id: " + id + " deleted successfully");
    return body;
  }


  public Map<String, Object> removeAll() {
    repo.deleteAll();

    Map<String, Object> body = new LinkedHashMap<>();
    body.put("message", "All employees deleted successfully");
    return body;
  }


  //the employee who has the biggest salary in the given department
  public Employee maxSalary(String department) {

    List<Employee> list = repo.findByDepartment(department);
    Employee employee = Employee.builder().build();

    if (list.isEmpty()) {
      throw new ResourceNotFoundException(
          "Could not found department with the name: " + department);
    }

    double max = list.get(0).getSalary();
    for (Employee e : list) {
      if (max <= e.getSalary()) {
        max = e.getSalary();
        employee = e;
      }
    }

    return employee;
  }

  //the manager who has the most "direct" employees coordinated by him
  public Employee getDirectManager() {

    List<Employee> employeeList = getAll();
    List<String> directManagerList = new ArrayList<>();

    for (Employee e : employeeList) {
      directManagerList.add(e.getDirectManager());
    }

    Employee manager = Employee.builder().build();

    //find the most repeated manager in direct_manager column
    Map<String, Integer> map = new HashMap<>();

    for (String st : directManagerList) {
      String key = st.toLowerCase();
      if (map.get(key) != null) {
        Integer value = map.get(key) + 1;
        map.put(key, value);
      } else {
        map.put(key, 1);
      }
    }

    //find the key which has the maximum value from the map
    String maxEntry = Collections.max(map.entrySet(), Map.Entry.comparingByValue()).getKey();

    for (Employee e : employeeList) {
      if (maxEntry.equalsIgnoreCase(e.getDirectManager())) {
        manager = e;
      }
    }

    return manager;
  }


  //BONUS POINTS
  //load a json file
  public Map<String, Object> importJsonFile() {

    Map<String, Object> body = new LinkedHashMap<>();

    InputStream stream = CRUDServiceApplication.class
        .getClassLoader()
        .getResourceAsStream("employees_93.json");

    //Read each line of the json file. Each file is one observation document.
    List<Document> observationDocuments = new ArrayList<>();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
      String line;
      while ((line = br.readLine()) != null) {
        observationDocuments.add(Document.parse(line));
      }
    } catch (IOException ex) {
      body.put("exception", ex.getMessage());
    }

    if (!observationDocuments.isEmpty()) {
      mongoTemplate.getCollection("employees").insertMany(observationDocuments);
      body.put("message",
          "Json file imported successfully, " + observationDocuments.size() + " docs imported");
    }

    return body;
  }


  //paging
  public Page<Employee> getPage(int page, int size) {

    Pageable pageable = PageRequest.of(page, size);
    return repo.findAll(pageable);
  }


  //top n best paid employees in a given department
  public List<Employee> topBestPaid(String department, int n) {

    Criteria find = Criteria.where("department").is(department);
    Query query =
        new Query().addCriteria(find).with(Sort.by(Sort.Direction.DESC, "salary")).limit(n);
    List<Employee> employeeList = mongoTemplate.find(query, Employee.class);

    if (employeeList.isEmpty()) {
      throw new ResourceNotFoundException(
          "Could not found department with the name: " + department);
    }

    return employeeList;
  }


  //management tree: from the top CEO to the lowest employee
  public List<Employee> managementTree() {

    Query query = new Query().with(Sort.by(Sort.Direction.DESC, "salary"));
    return mongoTemplate.find(query, Employee.class);
  }
}