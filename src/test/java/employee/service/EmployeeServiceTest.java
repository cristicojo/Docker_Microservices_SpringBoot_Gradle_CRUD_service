package employee.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import employee.entity.Employee;
import employee.repository.EmployeeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {

  // for the m1 and m2 mac processors
  static {
    System.setProperty("os.arch", "i686_64");
  }

  @Mock
  private EmployeeRepository repository;

  @Mock
  private MongoTemplate mongoTemplate;

  @InjectMocks
  private EmployeeService service;



  @Before
  public void setUp() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
  }


  @Test
  public void findAllEmployeesTest() {

    LocalDate date = LocalDate.of(1984, 11, 25);

    Employee eMock1 = new Employee();
    eMock1.setId("1");
    eMock1.setFirstName("testeeee 1");
    eMock1.setLastName("testeeee 1");
    eMock1.setDob(date.toString());
    eMock1.setDirectManager("TESTE 1");
    eMock1.setSalary(444.44);
    eMock1.setDepartment("testeeee 1");

    Employee eMock2 = new Employee();
    eMock2.setId("2");
    eMock2.setFirstName("testeeee 2");
    eMock2.setLastName("testeeee 2");
    eMock2.setDob(date.toString());
    eMock2.setDirectManager("TESTE 2");
    eMock2.setSalary(444.45);
    eMock2.setDepartment("testeeee 2");

    Employee eMock3 = new Employee();
    eMock3.setId("3");
    eMock3.setFirstName("testeeee 3");
    eMock3.setLastName("testeeee 3");
    eMock3.setDob(date.toString());
    eMock3.setDirectManager("TESTE 3");
    eMock3.setSalary(444.46);
    eMock3.setDepartment("testeeee 3");

    List<Employee> listMockEmployees = new ArrayList<>();
    listMockEmployees.add(eMock1);
    listMockEmployees.add(eMock2);
    listMockEmployees.add(eMock3);

    when(repository.findAll()).thenReturn((listMockEmployees));
    List<Employee> employeeServiceList = service.getAll();

    assertNotNull(employeeServiceList);
    assertEquals(listMockEmployees.get(0).getId(), employeeServiceList.get(0).getId());
    assertEquals(listMockEmployees.get(0).getFirstName(),
        employeeServiceList.get(0).getFirstName());
    assertEquals(listMockEmployees.get(0).getDirectManager(),
        employeeServiceList.get(0).getDirectManager());

    assertEquals(listMockEmployees.get(1).getDob(), employeeServiceList.get(1).getDob());
    assertEquals(listMockEmployees.get(1).getSalary(),
        employeeServiceList.get(1).getSalary(), .1);
    assertEquals(listMockEmployees.get(1).getDepartment(),
        employeeServiceList.get(1).getDepartment());

    assertEquals(listMockEmployees.get(2).getId(), employeeServiceList.get(2).getId());
    assertEquals(listMockEmployees.get(2).getFirstName(),
        employeeServiceList.get(2).getFirstName());
    assertEquals(listMockEmployees.get(2).getDirectManager(),
        employeeServiceList.get(2).getDirectManager());

  }

  @Test
  public void findEmployeeByIdTest() {

    LocalDate date = LocalDate.of(1984, 11, 25);
    Employee eMock = new Employee();

    eMock.setId("11");
    eMock.setFirstName("cristi teste");
    eMock.setLastName("cojocaru teste");
    eMock.setDob(date.toString());
    eMock.setDirectManager("TESTE");
    eMock.setSalary(444.44);
    eMock.setDepartment("IT");

    //inject mock object in the service
    when(repository.findById(((anyString())))).thenReturn(Optional.of(eMock));
    Employee employeeService = service.getById("11");

    assertNotNull(employeeService);
    assertEquals("11", eMock.getId());
    assertEquals(eMock.getFirstName(), employeeService.getFirstName());
    assertEquals(eMock.getDirectManager(), employeeService.getDirectManager());

  }


  @Test
  public void saveEmployeeTest() {

    LocalDate date = LocalDate.of(1984, 11, 25);
    Employee eMock = new Employee();

    eMock.setId("11");
    eMock.setFirstName("cristi teste");
    eMock.setLastName("cojocaru teste");
    eMock.setDob(date.toString());
    eMock.setDirectManager("TESTE");
    eMock.setSalary(444.44);
    eMock.setDepartment("IT");

    //inject mock object in the service
    when(repository.save((eMock))).thenReturn(eMock);
    Employee employeeService = service.create(eMock);

    assertNotNull(employeeService);
    assertEquals(eMock.getId(), employeeService.getId());
    assertEquals(eMock.getFirstName(), employeeService.getFirstName());
    assertEquals(eMock.getDirectManager(), employeeService.getDirectManager());

  }


  @Test
  public void saveEmployeeSTest() {

    LocalDate date = LocalDate.of(1984, 11, 25);

    Employee eMock1 = new Employee();
    eMock1.setId("1");
    eMock1.setFirstName("testeeee 1");
    eMock1.setLastName("testeeee 1");
    eMock1.setDob(date.toString());
    eMock1.setDirectManager("TESTE 1");
    eMock1.setSalary(444.44);
    eMock1.setDepartment("testeeee 1");

    Employee eMock2 = new Employee();
    eMock2.setId("2");
    eMock2.setFirstName("testeeee 2");
    eMock2.setLastName("testeeee 2");
    eMock2.setDob(date.toString());
    eMock2.setDirectManager("TESTE 2");
    eMock2.setSalary(444.45);
    eMock2.setDepartment("testeeee 2");

    Employee eMock3 = new Employee();
    eMock3.setId("3");
    eMock3.setFirstName("testeeee 3");
    eMock3.setLastName("testeeee 3");
    eMock3.setDob(date.toString());
    eMock3.setDirectManager("TESTE 3");
    eMock3.setSalary(444.46);
    eMock3.setDepartment("testeeee 3");

    List<Employee> listMockEmployees = new ArrayList<>();
    listMockEmployees.add(eMock1);
    listMockEmployees.add(eMock2);
    listMockEmployees.add(eMock3);

    when(repository.saveAll(listMockEmployees)).thenReturn(listMockEmployees);
    List<Employee> employeeServiceList = service.createAll(listMockEmployees);

    assertNotNull(employeeServiceList);
    assertEquals(listMockEmployees.get(0).getId(), employeeServiceList.get(0).getId());
    assertEquals(listMockEmployees.get(0).getFirstName(),
        employeeServiceList.get(0).getFirstName());
    assertEquals(listMockEmployees.get(0).getDirectManager(),
        employeeServiceList.get(0).getDirectManager());

    assertEquals(listMockEmployees.get(1).getDob(), employeeServiceList.get(1).getDob
        ());
    assertEquals(listMockEmployees.get(1).getSalary(),
        employeeServiceList.get(1).getSalary(), .1);
    assertEquals(listMockEmployees.get(1).getDepartment(),
        employeeServiceList.get(1).getDepartment());

    assertEquals(listMockEmployees.get(2).getId(), employeeServiceList.get(2).getId());
    assertEquals(listMockEmployees.get(2).getFirstName(),
        employeeServiceList.get(2).getFirstName());
    assertEquals(listMockEmployees.get(2).getDirectManager(),
        employeeServiceList.get(2).getDirectManager());
  }


  @Test
  public void updateEmployeeTest() {

    Employee oldEmployee = new Employee();
    oldEmployee.setId("11");
    oldEmployee.setFirstName("cristi teste");
    oldEmployee.setLastName("cojocaru teste");
    oldEmployee.setDirectManager("TESTE");
    oldEmployee.setSalary(444.44);
    oldEmployee.setDepartment("IT");

    Employee newEmployee = new Employee();
    newEmployee.setId("11");
    newEmployee.setFirstName("cristi2 teste");
    newEmployee.setLastName("cojocaru2 teste");
    newEmployee.setDirectManager("TESTE2");
    newEmployee.setSalary(555.55);
    newEmployee.setDepartment("IT-IT");



    //inject mock object in the service
    when(repository.findById(oldEmployee.getId())).thenReturn(Optional.of(oldEmployee));
    when(repository.save(oldEmployee)).thenReturn(oldEmployee);
    Employee employeeService = service.updateById(oldEmployee.getId(), newEmployee);

    assertNotNull(employeeService);
    assertEquals(newEmployee.getId(), employeeService.getId());
    assertEquals(newEmployee.getFirstName(), employeeService.getFirstName());
    assertEquals(newEmployee.getLastName(), employeeService.getLastName());
    assertEquals(newEmployee.getDirectManager(), employeeService.getDirectManager());
    assertEquals(newEmployee.getSalary(), employeeService.getSalary());
  }


  @Test
  public void deleteByIdTest() {

    Employee eMock = new Employee();

    eMock.setId("11");
    eMock.setFirstName("cristi teste");
    eMock.setLastName("cojocaru teste");
    eMock.setDirectManager("TESTE");
    eMock.setSalary(444.44);
    eMock.setDepartment("IT");

    Map<String, Object> map = new HashMap<>();
    map.put("message", "Employee with id: " + eMock.getId() + " deleted successfully");

    when(repository.findById(eMock.getId())).thenReturn(Optional.of(eMock));
    assertTrue(service.remove(eMock.getId())

        .get("message")
        .toString()
        .contains(map.get("message")
            .toString()));

  }

  @Test
  public void deleteAllTest() {

    Employee eMock = new Employee();

    eMock.setId("11");
    eMock.setFirstName("cristi teste");
    eMock.setLastName("cojocaru teste");
    eMock.setDirectManager("TESTE");
    eMock.setSalary(444.44);
    eMock.setDepartment("IT");

    Map<String, Object> map = new HashMap<>();
    map.put("message", "All employees deleted successfully");

    assertTrue(service.removeAll()

        .get("message")
        .toString()
        .contains(map.get("message")
            .toString()));

  }


  @Test(expected = ResourceNotFoundException.class)
  public void findEmployeeByIdNegativeTest() {

    Employee eMock = new Employee();
    eMock.setId("11");
    eMock.setFirstName("cristi teste");
    eMock.setLastName("cojocaru teste");
    eMock.setDirectManager("TESTE");
    eMock.setSalary(444.44);
    eMock.setDepartment("IT");

    when(repository.findById(eMock.getId())).thenReturn(Optional.empty());
    service.getById(eMock.getId());
  }


  @Test
  public void maxSalaryTest() {


    LocalDate date = LocalDate.of(1984, 11, 25);

    Employee eMock0 = new Employee();
    eMock0.setId("1");
    eMock0.setFirstName("testeeee 1");
    eMock0.setLastName("testeeee 1");
    eMock0.setDob(date.toString());
    eMock0.setDirectManager("TESTE 1");
    eMock0.setSalary(3.3);
    eMock0.setDepartment("dep 1");

    Employee eMock1 = new Employee();
    eMock1.setId("2");
    eMock1.setFirstName("testeeee 2");
    eMock1.setLastName("testeeee 2");
    eMock1.setDob(date.toString());
    eMock1.setDirectManager("TESTE 2");
    eMock1.setSalary(2.2);
    eMock1.setDepartment("dep 1");

    Employee eMock2 = new Employee();
    eMock2.setId("3");
    eMock2.setFirstName("testeeee 3");
    eMock2.setLastName("testeeee 3");
    eMock2.setDob(date.toString());
    eMock2.setDirectManager("TESTE 3");
    eMock2.setSalary(1.1);
    eMock2.setDepartment("dep 1");


    Employee eMock3 = new Employee();
    eMock3.setId("1");
    eMock3.setFirstName("testeeee 1");
    eMock3.setLastName("testeeee 1");
    eMock3.setDob(date.toString());
    eMock3.setDirectManager("TESTE 1");
    eMock3.setSalary(6.6);
    eMock3.setDepartment("dep 2");

    Employee eMock4 = new Employee();
    eMock4.setId("2");
    eMock4.setFirstName("testeeee 2");
    eMock4.setLastName("testeeee 2");
    eMock4.setDob(date.toString());
    eMock4.setDirectManager("TESTE 2");
    eMock4.setSalary(5.5);
    eMock4.setDepartment("dep 2");

    Employee eMock5 = new Employee();
    eMock5.setId("3");
    eMock5.setFirstName("testeeee 3");
    eMock5.setLastName("testeeee 3");
    eMock5.setDob(date.toString());
    eMock5.setDirectManager("TESTE 3");
    eMock5.setSalary(4.4);
    eMock5.setDepartment("dep 2");

    List<Employee> listMockEmployeeDep = new ArrayList<>();
    listMockEmployeeDep.add(eMock0);
    listMockEmployeeDep.add(eMock1);
    listMockEmployeeDep.add(eMock2);
    listMockEmployeeDep.add(eMock3);
    listMockEmployeeDep.add(eMock4);
    listMockEmployeeDep.add(eMock5);

    when(repository.findByDepartment(eMock3.getDepartment())).thenReturn(listMockEmployeeDep);
    Employee employeeService = service.maxSalary(eMock3.getDepartment());

    assertEquals(eMock3.getSalary(), employeeService.getSalary(), .1);
  }


  @Test
  public void getDirectManagerTest() {


    LocalDate date = LocalDate.of(1984, 11, 25);

    Employee eMock0 = new Employee();
    eMock0.setId("1");
    eMock0.setFirstName("testeeee 1");
    eMock0.setLastName("testeeee 1");
    eMock0.setDob(date.toString());
    eMock0.setDirectManager("TESTE 2");
    eMock0.setSalary(3.3);
    eMock0.setDepartment("dep 1");

    Employee eMock1 = new Employee();
    eMock1.setId("2");
    eMock1.setFirstName("testeeee 2");
    eMock1.setLastName("testeeee 2");
    eMock1.setDob(date.toString());
    eMock1.setDirectManager("TESTE 2");
    eMock1.setSalary(2.2);
    eMock1.setDepartment("dep 1");

    Employee eMock2 = new Employee();
    eMock2.setId("3");
    eMock2.setFirstName("testeeee 3");
    eMock2.setLastName("testeeee 3");
    eMock2.setDob(date.toString());
    eMock2.setDirectManager("TESTE 2");
    eMock2.setSalary(1.1);
    eMock2.setDepartment("dep 1");


    Employee eMock3 = new Employee();
    eMock3.setId("1");
    eMock3.setFirstName("testeeee 1");
    eMock3.setLastName("testeeee 1");
    eMock3.setDob(date.toString());
    eMock3.setDirectManager("TESTE 4");
    eMock3.setSalary(6.6);
    eMock3.setDepartment("dep 2");

    Employee eMock4 = new Employee();
    eMock4.setId("2");
    eMock4.setFirstName("testeeee 2");
    eMock4.setLastName("testeeee 2");
    eMock4.setDob(date.toString());
    eMock4.setDirectManager("TESTE 5");
    eMock4.setSalary(5.5);
    eMock4.setDepartment("dep 2");

    Employee eMock5 = new Employee();
    eMock5.setId("3");
    eMock5.setFirstName("testeeee 3");
    eMock5.setLastName("testeeee 3");
    eMock5.setDob(date.toString());
    eMock5.setDirectManager("TESTE 5");
    eMock5.setSalary(4.4);
    eMock5.setDepartment("dep 2");

    List<Employee> listMockEmployees = new ArrayList<>();
    listMockEmployees.add(eMock0);
    listMockEmployees.add(eMock1);
    listMockEmployees.add(eMock2);
    listMockEmployees.add(eMock3);
    listMockEmployees.add(eMock4);
    listMockEmployees.add(eMock5);

    when(repository.findAll()).thenReturn(listMockEmployees);
    Employee employeeService = service.getDirectManager();

    assertEquals(eMock0.getDirectManager().toLowerCase(),
        employeeService.getDirectManager().toLowerCase());
  }


  //bonus points
  @Test
  public void getPageTest() {

    //first choice
    LocalDate date = LocalDate.of(1984, 11, 25);

    Employee eMock1 = new Employee();
    eMock1.setId("1");
    eMock1.setFirstName("testeeee 1");
    eMock1.setLastName("testeeee 1");
    eMock1.setDob(date.toString());
    eMock1.setDirectManager("TESTE 2");
    eMock1.setSalary(3.3);
    eMock1.setDepartment("dep 1");

    Employee eMock2 = new Employee();
    eMock2.setId("2");
    eMock2.setFirstName("testeeee 1");
    eMock2.setLastName("testeeee 1");
    eMock2.setDob(date.toString());
    eMock2.setDirectManager("TESTE 2");
    eMock2.setSalary(3.3);
    eMock2.setDepartment("dep 1");

    Employee eMock3 = new Employee();
    eMock3.setId("3");
    eMock3.setFirstName("testeeee 1");
    eMock3.setLastName("testeeee 1");
    eMock3.setDob(date.toString());
    eMock3.setDirectManager("TESTE 2");
    eMock3.setSalary(3.3);
    eMock3.setDepartment("dep 1");

    List<Employee> employeeListMock = new ArrayList<>();
    employeeListMock.add(eMock1);
    employeeListMock.add(eMock2);
    employeeListMock.add(eMock3);

    int pageNumber = 1;
    //3 elem per page
    int pageSize = 3;

    Page<Employee> employeesPageMock = new PageImpl<>(employeeListMock);
    when(repository.findAll(PageRequest.of(pageNumber, pageSize))).thenReturn(employeesPageMock);
    Page<Employee> employeesPageController = service.getPage(pageNumber,
        pageSize);
    assertEquals(employeesPageMock.getNumberOfElements(),
        employeesPageController.getNumberOfElements());

  }


  @Test
  public void topNBestTest() {

    LocalDate date = LocalDate.of(1984, 11, 25);

    Employee eMock1 = new Employee();
    eMock1.setId("1");
    eMock1.setFirstName("testeeee 1");
    eMock1.setLastName("testeeee 1");
    eMock1.setDob(date.toString());
    eMock1.setDirectManager("TESTE 2");
    eMock1.setSalary(3.3);
    eMock1.setDepartment("dep 1");

    Employee eMock2 = new Employee();
    eMock2.setId("2");
    eMock2.setFirstName("testeeee 1");
    eMock2.setLastName("testeeee 1");
    eMock2.setDob(date.toString());
    eMock2.setDirectManager("TESTE 2");
    eMock2.setSalary(4.4);
    eMock2.setDepartment("dep 1");

    Employee eMock3 = new Employee();
    eMock3.setId("3");
    eMock3.setFirstName("testeeee 1");
    eMock3.setLastName("testeeee 1");
    eMock3.setDob(date.toString());
    eMock3.setDirectManager("TESTE 2");
    eMock3.setSalary(5.5);
    eMock3.setDepartment("dep 1");

    List<Employee> employeeListMock = new ArrayList<>();
    employeeListMock.add(eMock1);
    employeeListMock.add(eMock2);
    employeeListMock.add(eMock3);

    Criteria find = Criteria.where("department").is(eMock1.getDepartment());
    Query query =
        new Query().addCriteria(find).with(Sort.by(Sort.Direction.DESC, "salary")).limit(2);

    when(mongoTemplate.find(query, Employee.class)).thenReturn(employeeListMock);
    List<Employee> employeeServiceList = service.topBestPaid(eMock1.getDepartment(), 2);

    assertEquals(eMock2.getSalary(), employeeServiceList.get(1).getSalary(), .1);
    assertEquals(eMock3.getSalary(), employeeServiceList.get(2).getSalary(), .1);

  }


  @Test
  public void managementTreeTest() {

    LocalDate date = LocalDate.of(1984, 11, 25);

    Employee eMock1 = new Employee();
    eMock1.setId("1");
    eMock1.setFirstName("testeeee 1");
    eMock1.setLastName("testeeee 1");
    eMock1.setDob(date.toString());
    eMock1.setDirectManager("TESTE 2");
    eMock1.setSalary(3.3);
    eMock1.setDepartment("dep 1");

    Employee eMock2 = new Employee();
    eMock2.setId("2");
    eMock2.setFirstName("testeeee 1");
    eMock2.setLastName("testeeee 1");
    eMock2.setDob(date.toString());
    eMock2.setDirectManager("TESTE 2");
    eMock2.setSalary(6.6);
    eMock2.setDepartment("dep 1");

    Employee eMock3 = new Employee();
    eMock3.setId("3");
    eMock3.setFirstName("testeeee 1");
    eMock3.setLastName("testeeee 1");
    eMock3.setDob(date.toString());
    eMock3.setDirectManager("TESTE 2");
    eMock3.setSalary(5.5);
    eMock3.setDepartment("dep 1");

    List<Employee> employeeListMock = new ArrayList<>();
    employeeListMock.add(eMock1);
    employeeListMock.add(eMock2);
    employeeListMock.add(eMock3);

    Query query = new Query().with(Sort.by(Sort.Direction.DESC, "salary"));

    when(mongoTemplate.find(query, Employee.class)).thenReturn(employeeListMock);
    List<Employee> employeeServiceList = service.managementTree();

    assertEquals(eMock1.getSalary(), employeeServiceList.get(0).getSalary(), .1);
    assertEquals(eMock2.getSalary(), employeeServiceList.get(1).getSalary(), .1);
    assertEquals(eMock3.getSalary(), employeeServiceList.get(2).getSalary(), .1);

  }

  @Test(expected = ResourceNotFoundException.class)
  public void maxSalaryNegativeTest() {

    Employee eMock = new Employee();
    eMock.setId("11");
    eMock.setFirstName("cristi teste");
    eMock.setLastName("cojocaru teste");
    eMock.setDirectManager("TESTE");
    eMock.setSalary(444.44);
    eMock.setDepartment("IT");

    when(repository.findByDepartment(eMock.getDepartment())).thenReturn(Collections.emptyList());
    service.maxSalary(eMock.getDepartment());

  }

  @Test(expected = ResourceNotFoundException.class)
  public void topBestPaidNegativeTest() {

    Employee eMock = new Employee();
    eMock.setId("11");
    eMock.setFirstName("cristi teste");
    eMock.setLastName("cojocaru teste");
    eMock.setDirectManager("TESTE");
    eMock.setSalary(444.44);
    eMock.setDepartment("IT");

    Criteria find = Criteria.where("department").is(eMock.getDepartment());
    Query query =
        new Query().addCriteria(find).with(Sort.by(Sort.Direction.DESC, "salary")).limit(2);

    when(mongoTemplate.find(query, Employee.class)).thenReturn(Collections.emptyList());
    service.topBestPaid(eMock.getDepartment(), 2);

  }

//  @Test
//  public void importJsonFileTest() throws IOException {
//
//    Net net = new Net(Network.getFreeServerPort(), Network.localhostIsIPv6());
//    IMongodConfig
//        mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION).net(net)
//        .build();
//    IRuntimeConfig runtimeConfig = new RuntimeConfigBuilder().defaults(Command.MongoD).build();
//    MongodExecutable mongodExe = MongodStarter.getInstance(runtimeConfig).prepare(mongodConfig);
//    MongodProcess mongod = mongodExe.start();
//    File jsonFile = new File(Thread.currentThread()
//        .getContextClassLoader()
//        .getResource("employees_9.json")
//        .getFile());
//
//    String importDatabase = "local";
//    String importCollection = "employees2";
//    int port = 27017;
//
//    //import json file in the db
//    MongoImportExecutable
//        mongoImportExecutable =
//        mongoImportExecutable(port, importDatabase, importCollection, jsonFile.getAbsolutePath(),
//            true, true, true);
//    String hostName = net.getServerAddress().getHostName();
//    MongoClient mongoClient = MongoClients.create("mongodb://" + hostName + ":" + port);
//    MongoImportProcess mongoImportProcess = mongoImportExecutable.start();
//
//    assertEquals(10, mongoClient.getDatabase(importDatabase)
//        .getCollection(importCollection)
//        .countDocuments());
//    mongoImportProcess.stop();
//    mongod.stop();
//    mongodExe.stop();
//
//  }
//
//
//  private MongoImportExecutable mongoImportExecutable(int port, String dbName, String collection,
//      String jsonFile, Boolean jsonArray, Boolean upsert, Boolean drop) throws IOException {
//
//    IMongoImportConfig mongoImportConfig = new MongoImportConfigBuilder()
//        .version(Version.Main.PRODUCTION)
//        .net(new Net(port, Network.localhostIsIPv6()))
//        .db(dbName)
//        .collection(collection)
//        .upsert(upsert)
//        .dropCollection(drop)
//        .jsonArray(jsonArray)
//        .importFile(jsonFile)
//        .build();
//
//    return MongoImportStarter.getDefaultInstance().prepare(mongoImportConfig);
//  }
}